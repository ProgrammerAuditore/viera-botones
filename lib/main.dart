import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Color.fromARGB(255, 255, 255, 255),
      ),
      home: Scaffold(
        appBar: AppBar(
                centerTitle: true,
                title: Text('MySQL'),
                leading: IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.home),
                ),
                actions: [
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.search),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.more_vert),
                  ),
                ],
        ),
        body: Center(
         child: Container(
          child: Text(
            'MySQL es un sistema de gestión de bases de datos relacional desarrollado bajo licencia dual: Licencia pública general/Licencia comercial por Oracle Corporation y está considerada como la base de datos de código abierto más popular del mundo,1​2​ y una de las más populares en general junto a Oracle y Microsoft SQL Server, todo para entornos de desarrollo web.',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.bold,
              color: Colors.deepOrange,
              backgroundColor: Colors.amber
            ),
          ),
        ),
        ),

        // * Boton flotante
        floatingActionButton: FloatingActionButton.extended(
          onPressed: (){}, 
          label: Text('Crear tarea'),
          icon: const Icon(Icons.add_outlined),
          backgroundColor: Colors.green,
          tooltip: 'Nueva tarea',
          foregroundColor: Colors.white,
        ),

        // * Navegación bar
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home, 
                  size: 60, 
                  color: Colors.white,
                ),
                label: 'Inicio',
                backgroundColor: Colors.cyan
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.task),
                label: 'Tareas',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.book),
                label: 'Libretas',
                backgroundColor: Colors.amber
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                label: 'Settings',
                backgroundColor: Colors.pink,
              ),
            ],
            selectedItemColor: Colors.green[800]
          ),

      ),
    );
  }
}